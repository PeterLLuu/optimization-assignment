﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    Camera mainCam;
    NavMeshAgent navAgent;
    RaycastHit hit;

    private void Start()
    {
        mainCam = Camera.main.GetComponent<Camera>();
        navAgent = GetComponent<NavMeshAgent>();
        hit = new RaycastHit();
    }

    // Update is called once per frame
    void Update ()
    {
        if ( Physics.Raycast(mainCam.ScreenPointToRay( Input.mousePosition ), out hit, 100, LayerMask.GetMask( "Ground" ) ) )
        {
            navAgent.destination = hit.point;
        }
	}
}
