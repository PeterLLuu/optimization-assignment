﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;
    
    public List<GameObject> pool = new List<GameObject>();

    // Update is called once per frame
    void Update()
    {
        //If mouse 1 is pressed, Instanitate the ball prefab at camera position
        //Ball's direction is determined by converting screen space mouse position to world space
        //Then set the velocity of the ball to FireSpeed towards FireDirection
        if ( Input.GetButton( "Fire1" ) )
        {
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            Vector3 FireDirection = clickPoint - this.transform.position;
            FireDirection.Normalize();

            GameObject prefabInstance = GetGameObjectFromPool();
            prefabInstance.transform.position = this.transform.position;
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;

            print(prefabInstance);

            if (null == prefabInstance.GetComponent<Ball>())
            {
                prefabInstance.AddComponent<Ball>();
            }
        }
    }

    GameObject GetGameObjectFromPool()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if(!pool[i].activeInHierarchy)
            {
                pool[i].SetActive(true);
                return pool[i];
            }
        }

        GameObject obj = Instantiate(Prefab);
        pool.Add(obj);
        return obj;
    }

}

class Ball : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            gameObject.SetActive(false);
        }
    }
}